require 'google/apis/drive_v3'
require 'mail'
require 'net/imap'
require 'db'

class AlgorithmsHomeworkFetcher
  def initialize(gmail_login:, gmail_password:, google_json_key_io:, database_url:)
    @gmail_login = gmail_login
    @gmail_password = gmail_password
    @google_authorization = Google::Auth::ServiceAccountCredentials.make_creds(
      json_key_io: google_json_key_io,
      scope: Google::Apis::DriveV3::AUTH_DRIVE,
    )
    @google_drive = Google::Apis::DriveV3::DriveService.new
    @google_drive.authorization = @google_authorization
    @database_url = database_url
  end

  def call
    subject_id = DB.exec('SELECT id FROM subjects WHERE id = 5').values.first&.first || fail('No subject was found')
    each_mail do |seqno, mail|
      name = mail.subject.gsub(/домашнее задание/, 'ДЗ')
      two_weeks = mail.subject.include?('домашнее задание 7')
      exists = DB.exec_params(
        'SELECT EXISTS(SELECT * FROM task WHERE sub_id = $1 AND uid = $2)',
        [subject_id, seqno.to_s],
      ).values.first.first
      utc_offset = 3 * 60 * 60
      time = mail.date.to_time - utc_offset
      next_thursday = (time.to_date + 1 + ((3 - time.wday) % 7) + (two_weeks ? 7 : 0)).to_datetime.to_time - utc_offset
      values = {
        sub_id: subject_id,
        uid: seqno,
        name: name,
        start_date: time,
        soft_deadline: next_thursday - 1,
        hard_deadline: next_thursday + 16 * 60 * 60 + 50 * 60,
        status: 'New',
      }
      if exists
        update_task values
      else
        create_task(
          **values,
          google_drive_file_ids: mail.attachments.map do |attachment|
            @google_drive.create_file(
              { name: attachment.filename },
              fields: 'id',
              upload_source: StringIO.new(attachment.decoded),
            ).id.tap { |file_id| @google_drive.create_permission(file_id, { role: 'reader', type: 'anyone' }, {}) }
          end,
        )
      end
    end
  end

  def each_mail
    imap = imap()
    imap.uid_search(['SUBJECT', 'Алгоритмы'.b], 'UTF-8').each do |seqno|
      yield seqno, Mail.new(imap.uid_fetch(seqno, 'RFC822')[0].attr['RFC822'])
    end
  end

  def message_uids
    imap.uid_search(['SUBJECT', 'Алгоритмы'.b], 'UTF-8')
  end

  def list_files
    @google_drive.list_files
  end

  def imap
    imap = Net::IMAP.new('imap.gmail.com', 993, true)
    imap.login @gmail_login, @gmail_password
    imap.examine 'INBOX'
    imap
  end

  private

  def create_task(**values)
    DB.exec_params(<<~POSTGRESQL, values.values)
      INSERT INTO task (#{values.keys.join(',')}) VALUES (#{values.values.each_index.map(&:succ).join(',')})
    POSTGRESQL
  end

  def update_task(sub_id:, uid:, **values)
    DB.exec_params(<<~POSTGRESQL, [*values.values, sub_id, uid])
      UPDATE task SET #{values.keys.each_with_index.map { |key, index| "#{key}=$#{index + 1}" }.join(',')}
      WHERE sub_id = $#{values.length + 1} AND uid=$#{values.length + 2}
    POSTGRESQL
  end
end
