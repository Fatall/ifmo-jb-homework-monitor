require 'dotenv'
Dotenv.load File.expand_path('../../.env', __FILE__)
require 'pg'

DB = PG.connect(ENV.fetch('DATABASE_URL'))
DB.type_map_for_results = PG::BasicTypeMapForResults.new DB
DB.type_map_for_queries = PG::BasicTypeMapForQueries.new DB
