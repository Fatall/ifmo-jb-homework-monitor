require 'google/apis/calendar_v3'
require 'db'

class CreateCalendars
  def initialize(database_url:, google_json_key_io:, subjects_with_ids:, gmail_email:)
    @database_url = database_url
    @google_authorization = Google::Auth::ServiceAccountCredentials.make_creds(
      json_key_io: google_json_key_io,
      scope: [Google::Apis::CalendarV3::AUTH_CALENDAR],
    )
    @calendar_service = Google::Apis::CalendarV3::CalendarService.new
    @calendar_service.authorization = @google_authorization
    @subjects_with_ids = subjects_with_ids
    @gmail_email = gmail_email
  end

  def call
    @subjects_with_ids.each do |subject_name, subject_id|
      calendar_id = @calendar_service.insert_calendar({ summary: subject_name }, {}).id
      @calendar_service.insert_acl(calendar_id, { role: 'owner', scope: { type: 'user', value: @gmail_email } }, {})
      @calendar_service.insert_acl(calendar_id, { role: 'reader', scope: { type: 'default' } }, {})
      DB.exec_params(
        'INSERT INTO subjects (id, name, cal_id) VALUES ($1, $2, $3)',
        [subject_id, subject_name, calendar_id],
      )
    end
  end
end
