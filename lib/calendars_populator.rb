require 'google/apis/calendar_v3'
require 'google/apis/drive_v3'
require 'googleauth/stores/file_token_store'
require 'db'

class CalendarsPopulator
  def initialize(database_url:, google_json_key_io:)
    @database_url = database_url
    @google_authorization = Google::Auth::ServiceAccountCredentials.make_creds(
      json_key_io: google_json_key_io,
      scope: [Google::Apis::CalendarV3::AUTH_CALENDAR_EVENTS, Google::Apis::DriveV3::AUTH_DRIVE],
    )
    @calendar_service = Google::Apis::CalendarV3::CalendarService.new
    @calendar_service.authorization = @google_authorization
    @google_drive = Google::Apis::DriveV3::DriveService.new
    @google_drive.authorization = @google_authorization
  end

  def call
    DB.exec(
      'SELECT task.id, cal_id, cal_event_id, task.name, start_date, soft_deadline, hard_deadline,google_drive_file_ids, description
       FROM task JOIN subjects ON subjects.id = task.sub_id',
    ).values.each do |task_id, cal_id, cal_event_id, task_name, start_date, soft_deadline, hard_deadline, google_drive_file_ids, description|
      event = {
        summary: task_name,
        start: { date_time: start_date.to_datetime },
        end: { date_time: hard_deadline.to_datetime },
        description: [
          description,
          ("Мягкий дедлайн #{soft_deadline.to_time.localtime('+03:00').strftime('%Y-%m-%d %H:%M')}" if soft_deadline),
        ].compact.join("\n"),
        attachments: google_drive_file_ids.map do |file_id|
          file = @google_drive.get_file(file_id, fields: 'name,mime_type,web_view_link')
          { file_id: file_id, file_url: file.web_view_link, title: file.name, mime_type: file.mime_type }
        end,
      }
      if cal_event_id
        @calendar_service.patch_event(
          cal_id,
          cal_event_id,
          event,
          supports_attachments: true,
        )
      else
        cal_event_id = @calendar_service.insert_event(cal_id, event, supports_attachments: true).id
      end
      DB.exec_params(
        'UPDATE task SET status = $1, cal_event_id = $2 WHERE id = $3',
        ['OK', cal_event_id, task_id],
      )
    end
  end

  attr_reader :calendar_service

  def list_events(calendar_id)
    @calendar_service.list_events(calendar_id)
  end
end
