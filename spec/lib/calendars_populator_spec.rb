require 'calendars_populator'

RSpec.describe CalendarsPopulator do
  let(:calendars_populator) do
    described_class.new(
      database_url: ENV.fetch('DATABASE_URL'),
      google_json_key_io: StringIO.new(ENV.fetch('GOOGLE_JSON_KEY')),
    )
  end

  describe '#call' do
    subject { calendars_populator.call }

    it { subject }
  end
end
