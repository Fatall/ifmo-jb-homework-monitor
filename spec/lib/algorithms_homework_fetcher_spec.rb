require 'algorithms_homework_fetcher'

RSpec.describe AlgorithmsHomeworkFetcher do
  let(:algorithms) do
    described_class.new(
      gmail_login: ENV.fetch('GMAIL_LOGIN'),
      gmail_password: ENV.fetch('GMAIL_PASSWORD'),
      google_json_key_io: StringIO.new(ENV.fetch('GOOGLE_JSON_KEY')),
      database_url: ENV.fetch('DATABASE_URL'),
    )
  end

  describe '#call' do
    subject { algorithms.call }

    it { p subject }
  end

  describe '#list_files' do
    subject { algorithms.list_files }

    it { p subject }
  end
end
