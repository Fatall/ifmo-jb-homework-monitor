require 'create_calendars'

RSpec.describe CreateCalendars do
  let(:calendars_creator) do
    described_class.new(
      database_url: ENV.fetch('DATABASE_URL'),
      google_json_key_io: StringIO.new(ENV.fetch('GOOGLE_JSON_KEY')),
      subject_names: ['Алгоритмы', 'C++', 'Комбинаторика', 'Функциональное программирование'],
      gmail_email: "#{ENV.fetch('GMAIL_LOGIN')}@gmail.com",
    )
  end

  it { calendars_creator.call }
end
