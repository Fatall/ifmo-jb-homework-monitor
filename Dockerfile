FROM ubuntu
RUN apt-get update
RUN apt-get install -y git ruby-full build-essential libpq-dev
ADD . /app/
WORKDIR /app
RUN gem install bundler && bundle install --deployment
