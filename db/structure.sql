create table subjects (
    id            serial       primary key,
    name          varchar(255) not null unique,
    cal_id        varchar(255) not null
);

create type stype as ENUM('New', 'Update', 'OK');

create table task (
    id            serial       primary key,
    sub_id        int          references subjects not null,
    uid           varchar(255) NOT NULL,
    cal_event_id  varchar(255),
    name          varchar(255) not null,
    start_date    timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    soft_deadline timestamp,
    hard_deadline timestamp NOT NULL,
    status        stype        DEFAULT 'New' NOT NULL,
    google_drive_file_ids varchar(255)[] NOT NULL DEFAULT '{}',
    description   text,
    CHECK (start_date <= hard_deadline),
    unique (sub_id, uid)
);

CREATE FUNCTION up_task() RETURNS trigger AS $up_task$
    BEGIN
        IF OLD.status = 'OK' THEN
        New.status := 'Update';
        END IF;
        RETURN NEW;
    END;
$up_task$ LANGUAGE plpgsql;

CREATE TRIGGER up_task BEFORE UPDATE ON task
                                  FOR EACH ROW EXECUTE PROCEDURE up_task();
