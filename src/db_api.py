#!/usr/bin/python3

from datetime import timedelta
import os
import psycopg2
from urllib.parse import urlparse
from dotenv import load_dotenv

load_dotenv()


class Subjects:
    """ 
    ID subjects
    """

    @staticmethod
    def get_unix_id():
        return 1

    @staticmethod
    def get_cpp_id():
        return 2

    @staticmethod
    def get_fpz_id():
        return 3

    @staticmethod
    def get_fph_id():
        return 4

    @staticmethod
    def get_algorithm_id():
        return 5

    @staticmethod
    def get_os_id():
        return 6

    @staticmethod
    def get_db_id():
        return 7

    @staticmethod
    def get_graphp_id():
        return 8

    @staticmethod
    def get_graphs_id():
        return 9


class pdb:
    def __init__(self):
        url = urlparse(os.getenv("DATABASE_URL"))
        user = url.username
        password = url.password
        database = url.path[1:]
        host = url.hostname
        # self.db = psycopg2.connect("dbname='d9v23h58c539ol' user='kfqebxcdsuteax' host='ec2-46-137-75-170.eu-west-1.compute.amazonaws.com' password='42c3fb54f9db34b4e7a3b8373cf3688d51b5a2e835ef27664944e2c0f9e9ba3e'")
        self.db = psycopg2.connect(database=database, user=user,
                                   password=password, host=host)

    def check(self, sub, dzid, name, start, soft, hard, google=None, desk=None):
        delta = timedelta(hours=3)
        start = start - delta
        if soft:
            soft = soft - delta
        hard = hard - delta
        self.cur.execute("""SELECT start_date, soft_deadline, hard_deadline ,name ,description
                        FROM task WHERE sub_id = %(int)s AND uid = %(str)s
                        """,
                         {'int': sub, 'str': dzid})
        resall = self.cur.fetchall()
        if resall:
            res = list(resall[0])
            if res[1] != soft or res[2] != hard or res[3] != name or res[4] != desk:
                self.cur.execute("""
                    UPDATE task  SET name = %s, soft_deadline = %s,
                    hard_deadline = %s, google_drive_file_ids = %s,
                    description = %s WHERE sub_id = %s AND uid = %s;
                    """,
                                 (name, soft, hard, google, desk, sub, dzid))
                self.db.commit()
        else:
            self.cur.execute("""
                            INSERT INTO task (sub_id, uid, cal_event_id, name, 
                            start_date, soft_deadline, hard_deadline,
                            google_drive_file_ids, description)
                            VALUES (%s, %s, NULL, %s, %s, %s, %s, %s, %s);
                            """,
                             (sub, dzid, name, start, soft, hard, google, desk))
            self.db.commit()

    def __enter__(self):
        self.cur = self.db.cursor()
        return self

    def __exit__(self, type, value, trace):
        self.cur.close()
        self.db.close()


""" How use function check(...)"""
# if __name__ == '__main__':
#    with pdb() as db:
#        db.check(Subjects.get_unix_id(), 'dz2', datetime.datetime(2018,10,23,2,15,59,00,None), None, datetime.datetime(2018,10,31,2,15,59,00,None))
