"""Запрос информации о дедлайнах по Unix и скриптовым языкам"""
# coding=utf-8
import sys
import os
sys.path.append(os.path.dirname(__file__) + '/../')

import datetime
import json
import re
import requests
import db_api
from dotenv import load_dotenv
load_dotenv()


def unix_homework_request():
    url_branches_request = 'https://gitlab.com/api/v4/projects/scripting-languages%2F' \
                           'masters-2018%2Fpython-assignments/repository/branches'
    result = requests.get(url_branches_request, headers={"PRIVATE-TOKEN": os.getenv("GITLAB_TOKEN")})
    branches_data = json.loads(result.text)
    branches = [x['name'] for x in branches_data if "hw" in x['name']]

    url_readme_request = 'https://gitlab.com/api/v4/projects/scripting-languages%2F' \
                         'masters-2018%2Fpython-assignments/repository/files/' \
                         'README.md/raw'
    homeworks = []

    for x in branches:
        result = requests.get(url_readme_request,
                              headers={"PRIVATE-TOKEN": os.getenv("GITLAB_TOKEN")},
                              data={"ref": x})
        result.encoding = "utf-8"
        deadlines = re.search("\*\*Дедлайны\*\*: ([^(]+) \(мягкий\),"
                              " ([^(]+) \(жесткий\)", result.text)

        description = 'https://gitlab.com/scripting-languages/' \
                      'masters-2018/python-assignments/tree/' + x
        homeworks.append((x, "Python ДЗ " + x[2:], description,
                          datetime.datetime.strptime(deadlines.group(1),
                                                     '%Y.%m.%d %H:%M'),
                          datetime.datetime.strptime(deadlines.group(2),
                                                     '%Y.%m.%d %H:%M')
                          ))
    return homeworks


def main():
    with db_api.pdb() as db:
        for x in unix_homework_request():
            db.check(db_api.Subjects.get_unix_id(), x[0], x[1], x[3] - datetime.timedelta(3), x[3], x[4], [], x[2])


if __name__ == '__main__':
    main()
