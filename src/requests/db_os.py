#!/usr/bin/python3
import sys
import os

sys.path.append(os.path.dirname(__file__) + '/../')

from datetime import datetime, timedelta
from db_api import Subjects, pdb


def os_hw():
    cur_date = datetime(year=2018, month=9, day=5, hour=13, minute=40,
                        second=00)
    delta = timedelta(days=7)
    res = []
    for i in range(15):
        cur_date = cur_date + delta
        res.append(['OC ' + str(i + 1),
                    'https://stepik.org/lesson/54991/step/1?unit=32922, https://github.com/OSLL/ifmo-os-2018', cur_date,
                    cur_date + timedelta(minutes=1),
                    'hw ' + str(i + 1)])
    return res


def db_hw():
    cur_date = datetime(year=2018, month=9, day=5, hour=15, minute=20,
                        second=00)
    delta = timedelta(days=7)
    res = []
    for i in range(15):
        cur_date = cur_date + delta
        res.append(['БД ' + str(i + 1),
            'https://drive.google.com/drive/u/0/folders/1WutOrGFIcYnAzXGk6rClP8Auue8lM9xo, http://git.barashev.net/', cur_date,
                    cur_date + timedelta(minutes=1),
                    'hw ' + str(i + 1)])
    return res


def main():
    with pdb() as db:
        for x in os_hw():
            #        print (x)
            db.check(Subjects.get_os_id(), x[4], x[0], x[2], None, x[3], [],
                     x[1])
        for x in db_hw():
            #        print (x)
            db.check(Subjects.get_db_id(), x[4], x[0], x[2], None, x[3], [],
                     x[1])


if __name__ == '__main__':
    main()
