#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import os
sys.path.append(os.path.dirname(__file__) + '/../')

import psycopg2
import requests
import re
from datetime import datetime
from datetime import timedelta
from db_api import Subjects, pdb


def get_html(url):
    # html код этой страницы
    r = requests.get(url)
    return r.text


def act_month(name):
    if name == "сентября":
        return 9
    if name == "октября":
        return 10
    if name == "ноября":
        return 11
    if name == "декабря":
        return 12


def get_dates_samoilova(html):
    # (url, name, day, name_month)
    name = "Комбинаторика, ДЗ"
    dates_all = []
    dates = re.findall('<\/p><p><a rel="nofollow" class="external'
                       ' text" href="([^"]+)">([^<]+)<\/a> '
                       'Крайний срок сдачи: ([0-9]+) ([а-я]+)', html)

    for date in dates:
        date_end = datetime(day=int(date[2]), month=act_month(date[3]), year=2018, hour=23, minute=59, second=59)
        dates_all.append((date[0], name + date[1][16:], date_end - timedelta(6), None, date_end))
    return dates_all


def get_dates_paletskih(html):
    name = "Комбинаторика, ДЗ "
    #(url, name, day, name_month, hour, min, day_soft, month_soft, hour_soft, min_soft)
    dates_with_soft = re.findall('<\/p><p><a rel="nofollow" class="external text"'
                       ' href="([^"]+)">([^<]+)<\/a> Дедлайн: ([0-9]+)'
                       ' ([а-я]+) ([0-9]+):([0-9]+).'
                       ' Мягкий дедлайн: ([0-9]+) ([а-я]+) ([0-9]+):([0-9]+)', html)
    n = len(dates_with_soft)#count of tasks with soft dedline
    dates = re.findall('<\/p><p><a rel="nofollow" class="external'
                       ' text" href="([^"]+)">([^<]+)<\/a> Дедлайн:'
                       ' ([0-9]+) ([а-я]+) ([0-9]+):([0-9]+)', html)

    dates_all = [] #all tasks: (url, name, start, soft_dedline, hard_dedline)
    i = 0 #courrent number of task with soft dedline
    for date in dates:
        if i < n and dates_with_soft[i][1] != date[1]:
            date_end = datetime(day=int(date[2]), month=act_month(date[3]), year=2018, hour=int(date[4]),
                                minute=int(date[5]), second=0)
            dates_all.append((date[0], name + date[1][16:], date_end - timedelta(6), None, date_end))
        else:

            ds = dates_with_soft[i]
            date_soft_end = datetime(day=int(ds[6]), month=act_month(ds[7]), year=2018, hour=int(ds[8]),
                                     minute=int(ds[9]), second=0)
            date_end = datetime(day=int(ds[2]), month=act_month(ds[3]), year=2018, hour=int(ds[4]), minute=int(ds[5]),
                                second=0)
            dates_all.append((ds[0], name + ds[1][16:], date_soft_end - timedelta(2), date_soft_end, date_end))
            i += 1

    return dates_all


def main():
    """https://wiki.compscicenter.ru/index.php/Комбинаторика_и_теория_графов_5SE_осень_2018"""
    wiki = get_html("https://wiki.compscicenter.ru/index.php/Комбинаторика_и_теория_графов_5SE_осень_2018")
    HW_dates_Sam = get_dates_samoilova(wiki)
    HW_dates_Pal = get_dates_paletskih(wiki)
    count = 0

    with pdb() as db:
        for date in HW_dates_Pal:
            count += 1
            id_task = "id" + str(count) 
            db.check(Subjects.get_graphp_id(), id_task, date[1],
                 date[2], date[3], date[4], [], date[0])
        for date in HW_dates_Sam:
            count += 1
            id_task = "id" + str(count)
            db.check(Subjects.get_graphs_id(), id_task, date[1],
                 date[2], date[3], date[4], [], date[0])


if __name__ == '__main__':
    main()
