"""Запрос информации о дедлайнах по C++"""
# coding=utf-8
import sys
import os
sys.path.append(os.path.dirname(__file__) + '/../')

import datetime
import re
import requests
from slackclient import SlackClient
import db_api
from google.oauth2 import service_account
from googleapiclient.discovery import build
from dotenv import load_dotenv
import json
load_dotenv()

URL_REGEX = r"\b((?:https?://)?(?:(?:www\.)?(?:[\da-z\.-]+)\.(?:[a-z]{2,6})|(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)|(?:(?:[0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,7}:|(?:[0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,5}(?::[0-9a-fA-F]{1,4}){1,2}|(?:[0-9a-fA-F]{1,4}:){1,4}(?::[0-9a-fA-F]{1,4}){1,3}|(?:[0-9a-fA-F]{1,4}:){1,3}(?::[0-9a-fA-F]{1,4}){1,4}|(?:[0-9a-fA-F]{1,4}:){1,2}(?::[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:(?:(?::[0-9a-fA-F]{1,4}){1,6})|:(?:(?::[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(?::[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(?:ffff(?::0{1,4}){0,1}:){0,1}(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])|(?:[0-9a-fA-F]{1,4}:){1,4}:(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])))(?::[0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])?(?:/[\w\.-]*)*/?)\b"


def get_week_days_for_two_weeks(day, dt):
    dt = dt.replace(hour=13, minute=40, second=0)
    prev = dt - datetime.timedelta((day - dt.weekday()) % 7)
    return prev, prev + datetime.timedelta(7)


def c_plus_plus_homework_request():
    sc = SlackClient(os.getenv("SLACK_TOKEN"))

    results = sc.api_call(
        "channels.history",
        channel='CCR3YTPV0'
    )

    credentials = service_account.Credentials.from_service_account_info(
        json.loads(os.getenv("GOOGLE_JSON_KEY")),
        scopes=['https://www.googleapis.com/auth/drive.metadata.readonly'])

    service = build('drive', 'v3', credentials=credentials)

    homeworks = []

    if results['ok']:
        for result in results['messages']:
            if result['user'] == 'UCHJ5UA90' and result['type'] == 'message':
                for link in re.findall(URL_REGEX, result['text']):
                    if link.startswith('http'):
                        session = requests.Session()
                        resp = session.head(link, allow_redirects=True)
                        if 'https://docs.google.com/presentation/d/' in resp.url:
                            f_id = re.search("https:\/\/docs\.google\.com\/presentation\/d\/([^\/]+)", resp.url).group(1)
                            results = service.files().get(fileId=f_id).execute()
                            if results['name'].startswith('1.'):
                                dates = get_week_days_for_two_weeks(1, datetime.datetime.fromtimestamp(int(float(result['ts']))))
                                homeworks.append((results['name'], 'Дорешка ' + results['name'], f_id, dates[0], dates[1]))

    return homeworks


def main():
    with db_api.pdb() as db:
        for x in c_plus_plus_homework_request():
            db.check(db_api.Subjects.get_cpp_id(), x[0], x[1], x[3], None, x[4], [x[2]], None)


if __name__ == '__main__':
    main()
