#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import os
sys.path.append(os.path.dirname(__file__) + '/../')


import requests
import re
from datetime import datetime, timedelta
import psycopg2
from db_api import Subjects, pdb


def get_html(url):
    # html код этой страницы
    r = requests.get(url)
    return r.text


def get_dates_gavoronkov(html):
    #начало - дефолт
    #жесткий +1 сутки после мягкого
    #(url, name, start=now, soft, hard)
    name = "ФП, ДЗ "
    dates = re.findall('href="([^"]+)" class="internal" title="[^>]+>'
                       'Семинар и (домашнее задание [0-9]+)<\/a> \(<b>Дедлайн\(мягкий\):'
                      ' ([^<]+)', html)
    dates_all = []
    for date in dates:
        date_soft_end = datetime.strptime(date[2], "%d.%m.%Y %H:%M:%S")
        delta = timedelta(days=1)
        date_start = date_soft_end - timedelta(6)
        link = "http://wiki.compscicenter.ru" + date[0]
        dates_all.append((link, name + date[1][-1], date_start, date_soft_end, date_soft_end + delta))
    return dates_all


def get_dates_halndkiy(html):
    #название генерировать в цикле
    #возращает массив кортежей: (start,end)
    #для каждого 16:40 жесткий
    #за три дня до жестого мягкий
    #(url, name, start, soft, hard)
    name = "ФП, ДЗ "
    dates = re.findall('([0-9]{2}\.[0-9]{2})-([0-9]{2}\.[0-9]{2})'
                       ' -- <a rel="nofollow" class="external text" href="([^"]+)', html)
    i = 0# номер дз
    dates_all = []
    for date in dates:
        i += 1
        #datetime.strptime(date[0], "%d.%m")
        delta = timedelta(days=3)
        date_start = datetime(day=int(date[0][:2]), month=int(date[0][3:]), year=2018, hour=18, minute=10, second=00)
        date_end = datetime(day=int(date[1][:2]), month=int(date[1][3:]), year=2018, hour=16, minute=50, second=00)
        dates_all.append((date[2], name + str(i), date_start, date_end - delta, date_end))
    return dates_all


def main():
    """https://wiki.compscicenter.ru/index.php/ФП_5SE_осень_2018"""
    wiki = get_html("https://wiki.compscicenter.ru/index.php/ФП_5SE_осень_2018")
    HW_dates_Gav = get_dates_gavoronkov(wiki)
    HW_dates_Hal = get_dates_halndkiy(wiki)
    count = 0

    with pdb() as db:
        for date in HW_dates_Gav:
            count += 1
            id_task = "id" + str(count)
            db.check(Subjects.get_fpz_id(), id_task, date[1],
                 date[2], date[3], date[4], [], date[0])
        for date in HW_dates_Hal:
            count += 1
            id_task = "id" + str(count)
            db.check(Subjects.get_fph_id(), id_task, date[1],
                 date[2], date[3], date[4], [], date[0])


if __name__ == '__main__':
    main()
