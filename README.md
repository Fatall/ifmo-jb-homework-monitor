# ИТМО JetBrains 2018-2019. Мониторинг домашних заданий

## Требования

* Ruby >= 2.3
* Python 3
* PostgreSQL

## Конфигурация

* [Зарегистрируйте](https://accounts.google.com/signup/v2/webcreateaccount?flowName=GlifWebSignIn&flowEntry=SignUp) пользователя Google.
* [Включите](https://mail.google.com/mail/u/0/#settings/fwdandpop) IMAP
* [Создайте](https://console.cloud.google.com/projectcreate) Google Cloud проект 
* Включите [Google Drive](https://console.cloud.google.com/apis/library/drive.googleapis.com?q=drive&id=e44a1596-da14-427c-9b36-5eb6acce3775&folder&organizationId) и [Google Calendars](https://console.cloud.google.com/apis/library/calendar-json.googleapis.com?id=84f291c9-2585-4af1-a78b-09c53a78202f&folder&organizationId) в проекте
* [Создайте](https://console.cloud.google.com/iam-admin/serviceaccounts/create) сервисный аккаунт в проекте.
* Получите доступ к cpp-spb.slack.com. [Создайте](https://api.slack.com/) slack приложение и получите токен.
* Получите доступ к [группе](https://gitlab.com/scripting-languages/masters-2018) на GitLabe. [Получите](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) персональный токен.
* Задайте переменные окружения:

```bash
DATABASE_URL=
GITLAB_TOKEN=
GMAIL_LOGIN=
GMAIL_PASSWORD=
GOOGLE_JSON_KEY=
SLACK_TOKEN=
```

## Подготовка среды

```
bundle install --deployment
pip install -r requirements.txt
psql "$DATABASE_URL" -f db/structure.sql
bin/init
```

В вашем календаре появятся подкалендари для каждого предмета. Ими можно поделиться.

## Запуск синхронизации домашних заданий

```bash
bin/worker
``` 
